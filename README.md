# NI-MVI

This repository contains semester project assigned on NI-MVI in CTU in Prague.

## Assignment 

Main goal of this assignment is to take short video and increase its resolution using neural networks with Generative Adversarial Network (GAN) and U-Net architecture.
These two architectures should be then compared and commented.

## Repository content

Models can be find in Gan and Unet folders.
Utilities contains code for preprocessing, data handling and visualization.
To run models localy, please download weights and dataset linked below.

**NOTE**

Repository contains implementation copied from kaggle notebook. 
It is highly recomended to run it in some sort of notebook together.

## Aditional content

Link for notebook in kaggle [here](https://www.kaggle.com/adamformnek/ni-mvi-sr-using-gan-and-u-net) (for training).

Link for train dataset in kaggle [here](https://www.kaggle.com/adamformnek/tomandjerry-train).

Link for notebook in google colab [here](https://colab.research.google.com/drive/11os0ZvJ44Dsm6eOW3FlgTcbZHRoV7rD-?usp=sharing) (for plotting images and loading weights).

Link for weights, dataset, train history and results [here](https://drive.google.com/drive/folders/1DiiYQSCVySNR8_ijSyldXO9zbyU6TRk0?usp=sharing)
